package main

import (
	"fmt"
	r "runtime"
	"time"
)

const CONST_INT = 1

type Sample interface {
	ToString()
}

type Imp struct {
	id int
}

func (i *Imp) ToString() {
	fmt.Printf("%v", i.id)
}

func main() {

	interfaceSample(&Imp{id: 1})

	// arraySample()

	// pu := NewUser("太郎")
	// fmt.Printf("%T, %v, %v\n", pu, pu.name, pu.age)

	//methodSample()

	//structSample3()
	//structSample()

	//pointerSample()

	//chanelSample()

	// fmt.Println(sum(2, 3, 4, 5))

	// sliceSample()

	//strCopy()

	//goroutine()

	// fmt.Println(GetAppName())

	// a := rawInt(2)
	// fmt.Println("a", a)

	// b := func(x int) (y int) {
	// 	y = x + 2
	// 	return y
	// }
	// fmt.Println("b", b(1))

	// c := add(1)(2)
	// d := add(2)(3)
	// e := add(5)
	// f := e(10)
	// g := e(20)
	// fmt.Println("c", c, "d", d, "f", f, "g", g)

	// var varInt int64 = 2
	// if CONST_INT < varInt {
	// 	fmt.Println("超えた")
	// 	fmt.Printf("%T %T \n", CONST_INT, varInt)
	// }

	// iotaTest()
}

func interfaceSample(s Sample) {
	s.ToString()
}

func arraySample() {
	a := [3]int{1, 2, 3}
	b := [3]int{4, 5, 6}

	a = b // ここで参照を渡すのではなく、要素のコピーが発生

	a[0] = 10

	fmt.Printf("%v\n", a) // "[10,2,3]"
	fmt.Printf("%v\n", b) // "[4,5,6]"
}

func rawInt(x int) (result int) {
	result += x
	return result
}

func add(x int) func(int) int {
	xx := x
	return func(y int) int {
		return xx + y
	}
}

func iotaTest() {
	const i1 = iota
	const i2 = iota
	if i1 == i2 {
		fmt.Println("iotaTest 一致")
		return
	}
	fmt.Println("iotaTest 不一致")
}

func goroutine() {
	go fmt.Println("Year!")
	go fmt.Println("Year1!")
	go fmt.Println("Year2!")

	fmt.Printf("NumCPU: %d\n", r.NumCPU())
	fmt.Printf("NumGoroutine: %d\n", r.NumGoroutine())
	fmt.Printf("Version: %s\n", r.Version())
}

func strCopy() {
	b := make([]byte, 9)
	n := copy(b, "あいうえお")
	fmt.Println(b, n, string(b))
}

func sliceSample() {
	s1 := []int{1, 2, 3, 4, 5}
	s2 := s1[1:2]
	fmt.Println(s2, len(s2), cap(s2))
}

func sum(s ...int) int {
	n := 0
	for _, v := range s {
		n += v
	}

	return n
}

func chanelSample() {
	ch := make(chan int, 2)

	go reciever("1", ch)
	go reciever("2", ch)

	i := 0
	for i < 100 {
		ch <- i
		i++
	}
	close(ch)

	time.Sleep(5 * time.Second)
}

func reciever(name string, ch <-chan int) {
	for {
		i, ok := <-ch

		// 受信できなくなったら終了
		if ok == false {
			break
		}

		fmt.Printf("%v: %v\n", name, i)
	}
}

func pointerSample() {
	i := 1
	var p *int
	p = &i
	i2 := *p

	i = 10
	fmt.Printf("type: %T, value: %v\n", i, i)
	fmt.Printf("type: %T, value: %v\n", i2, i2)
}

func structSample() {
	type Z int
	type WithAlp struct {
		a int
		b int
		c int
	}
	type Point struct {
		x, y int
		Z
		WithAlp
	}

	pt := Point{
		// x
		x: 1,
		// y
		y: 2,
		WithAlp: WithAlp{
			a: 5,
		},
	}

	fmt.Println(pt.x, pt.y, pt.Z, pt.a)

	type Hoge struct {
		a string
	}

	type Fuga struct {
		a string
	}

	// hoge := Hoge{a: "hoge"}
	// fuga := Fuga{a: "fuga"}
	// hoge = fuga
	// fmt.Println(hoge)

	// type MyString string
	// var str string
	// str = "first"
	// fmt.Printf("%T, %v\n", str, str)
	// (func(s MyString) {
	// 	fmt.Printf("%T, %v\n", s, s)
	// })(str)

	type Child struct {
		c string
	}

	type Parent struct {
		pon string
		Child
	}

	parent := Parent{
		pon: "p",
		Child: Child{
			c: "c",
		},
	}

	(func(p Parent) {
		p.c = "func!"
	})(parent)
	fmt.Println(parent.c)
}

func structSample2() {
	type Person struct {
		age int
	}

	p := new(Person)

	fmt.Printf("%T, %v\n", p, p)

	p2 := &Person{}
	fmt.Printf("%T, %v\n", p2, p2)
}

func structSample3() {
	type hello map[int][]int32

	var key []int32

	hoge := &hello{}
	fmt.Println(hoge)

	fuga := new(hello)
	fmt.Println(fuga)

	(*hoge)[0] = key
	fmt.Println(hoge)

	(*fuga)[0] = key
	fmt.Println(fuga)
}

type MethodSample struct {
	X, Y int
}

func (m MethodSample) Sum() int {
	s := m.X + m.Y
	return s
}

func (m MethodSample) PlusOne() {
	m.X = m.X + 1
}

func (p *MethodSample) PlusOneRef() {
	p.X = p.X + 1
}

func methodSample() {
	m := MethodSample{
		X: 1,
		Y: 2,
	}

	m.PlusOne()
	fmt.Println(m)
	m.PlusOneRef()
	fmt.Println(m)

	// fmt.Println(m.Sum())
}

type User struct {
	name string
	age  int
}

func NewUser(name string) *User {
	user := User{
		name: name,
		age:  0,
	}

	return &user
}
