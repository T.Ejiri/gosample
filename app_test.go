package main

import (
	"testing"
)

/*
test
*/
func TestGetAppName(t *testing.T) {
	except := "gosample"
	actual := GetAppName()

	if except != actual {
		t.Errorf("%s != %s", except, actual)
	}
}
