# Goメモ

## 基本型
|||備考|
|---|---|---|
|論理値|bool||
|整数(符号付)|int8, int16, int32, int64||
|整数(符号なし)|uint8, uint16, uint32, uint64|byte型はuint8の別名|
|整数(実装依存)|int, uint, unitptr|32ビット実装 or 64ビット実装|
|浮動小数点|float32, float64|暗黙的な変数定義の場合、float64|
|複素数型|complex64, complex128|`実部 + 虚部i` で表現|
|ルーン||Unicodeコードポイントを表す整数型, int32の別名|
|文字列|string|・バックスラッシュを用いた場合、ルーン型と同義<br>・バッククォートを用いた場合、複数行に渡る文字列が書ける|

### キャスト
```go
n := 1 // int型
b := byte(n) // byte型へ変換
```

### interface{}型
あらゆる型と互換性がある。
初期値はnil。
このままでは演算はできない。

```go
var x interface{}
fmt.Printf("%#v", x) // "<nil>"
```

### 型アサーション
動的に変数の型をチェックする。

interface{}型はあらゆる型を取るため、
以下の記述で型を復元できる。


```go
var x interface{} = 3

i := x.(int) // iにはint型の3が入る
```

ただし、上記では型アサーションが失敗した場合エラーとなる。
ゆえに以下の書き方が望ましい。

```go
var x interface{} = 3

i, isInt := x.(int) // iにはint型の3が、isIntには型アサーションの成否が入る
```

型チェックはif文より、switchでかくと手軽に書ける(switchで後述)





## 変数
### 暗黙的な定義
初期化によって、varの省略と型定義を一度に行う。
```go
i := 1
```

まとめて定義する場合はvarを用いた方が見やすい。
```go
var (
    n = 1
    s = "string"
    b = true
)
```





## 定数
```go
const ONE = 1
```

### 型について
厳密には、型ありとなしで分かれている。

明示的に型ありにする場合、
```go
const I64 = int64(-1)
const F64 float64 = -1
```
のような書き方をする必要がある。
気にするのは整数型や、浮動小数点数型のときかな。


### iota
Goにenumは無いけど、iotaで似たようなことができる。
```go
const (
    A = iota // A == 0
    B        // B == 1
    C        // C == 2
)
```





## スコープ
パッケージ > ファイル > 関数 > ブロック > 制御構文

### パッケージ
変数、定数、関数問わず、
他のパッケージから参照可能かは`最初の一文字目が大文字かどうか`で判定される。

```go
const (
    A = 1       // 公開
    abc = "abs" // パッケージ内のみ参照可能
);
```

### ファイル
インポート宣言のみ独立している。

### ブロック
関数の中では`{}`を用いて、別のブロックを定義できる。
```go
func doSomething() {
    {
        var a int
    }
}
```





## パッケージ
### エイリアス
```go
import (
    f "fmt"
)
```

### 省略
```go
import (
    . "math"
)

fmt.Println(Pi)
```





## 配列
### 定義
```go
a := [5]int{1,2,3,4,5} // 型名は [5]int になる

i := [3]int{} // i == [0,0,0]
b := [3]bool{} // b == [false,false,false]
s := [3]string{} // b == ["","",""]

o := [...]int{1,2,3} // インデックスの省略、o := [3]int{1,2,3}と同義
```

### 参照
```go
a[0]
```

### 配列型の代入の動作はコピー
```go
a := [3]int{1,2,3}
b := [3]int{4,5,6}

a = b // ここで参照を渡すのではなく、要素のコピーが発生

a[0] = 10

fmt.Printf("%v\n", a) // "[10,5,6]"
fmt.Printf("%v\n", b) // "[4,5,6]"
```

### サイズの拡張・縮小
不可
-> スライスを使う





## 参照型
### make
参照型の生成に使用する関数。

|make|対応型|説明|
|:--|:--|:--|
|make(T)|スライス|-|
||マップ|作成|
||チャネル|バッファなしで作成|
|make(T, arg1)|スライス|要素数、容量共にarg1で作成|
||マップ|要素数arg1をヒントにして作成|
||チャネル|バッファサイズarg1で作成|
|make(T, arg1, arg2)|スライス|要素数arg1, 容量がarg2で作成|
||マップ|-|
||チャネル|-|

### スライス
可変長配列。
型は`[]T`で表す。
配列型を使い方が似ていることが多い。
配列型の違いの一種として、定義のみの場合スライスはnilになる。

```go
var s []int

s := make([]int, 10)
s[0] = 3
fmt.Println(s[0])

// 配列型風にスライスを作成
s2 := []int{1, 2, 3}

// 要素数を知る
len(s)

// 容量を知る
cap(s)

// 簡易スライス式
// [1]配列やスライスから新たなスライスを生成
s3 := s2[0:2] // index0からindex(2-1)までをコピーして生成
s4 := s2[1:] // index1からindex(len(s2)-1)まで(省略
s5 := s2[:2] // index0からまでindex(2-1)まで(省略
s6 := s2[:] // 全部を(省略

// [2]文字列をスライス式で切り出す
s7 := "ABCDE"[1:3] // "BC"

// 追加
s8 := []int{1, 2, 3}
s8 = append(s8, 4) // [1, 2, 3, 4]
s8 = append(s8, 5, 6, 7) // [1, 2, 3, 4, 5, 6, 7]

s9 := []int{8, 9}
s8 = append(s8, s9...) // [1, 2, 3, 4, 5, 6, 7, 8, 9]

// (上書き)コピー
// [1]基本
s10 := []int{1, 2, 3}
s11 := []int{10, 11}
succeccCount := copy(s10, s11) // succeccCount = 2, s10 = [10, 11, 3]

// [2]第１引数の要素数分しか適用されない例
s12 := []int{1, 2, 3, 4}
s13 := []int{10, 11, 12, 13, 14, 15}
succeccCount := copy(s12, s13) // succeccCount = 2, s10 = [10, 11, 12, 13]

// [3]文字列をbyteで切り出してコピー
b := make([]byte, 9)
succeccCount1 := copy(b, "あいうえお") // succeccCount1 = 9, b = [227, 129, ...]
s := string(b) // あいう
```

#### 容量
要素数と容量を別々に指定した場合、
次のようなイメージになる。

```
(要素数3, 容量5)
[0][0][0][][]
```

Goが確保したメモリ領域を超えた場合、
Goは新たな領域に必要な分だけ丸ごとコピーを行う。
これはコストが高い動作のため、
容量が推定できる場合は、事前にその分確保しておくと良い。

#### 完全スライス式
簡易スライス式との違いは、容量が自動で決まるか否か。

```go
// 配列型のイメージ -> [1][2][3][4][5][6][7][8][9][10]
var a = [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

// 簡易スライス式はlowを切り捨て、残りを容量にする
// cap(a) = len(a) - low
// [3][4][][][][][][]
a[2:4]

// 完全スライス式は容量を制御する
// (low:high:max)の関係で、cap(a) = max - low
// [3][4][][]
a[2:4:6]
```

#### スライスの注意点
参照型であるスライスは、
簡易スライス式によって、
容量が元の値と変わらない場合はメモリを参照しているが、
append()の影響で容量が拡張されたりすると、
メモリの参照が切れたりする。

### マップ
連想配列。
`map[キーの型]要素の型`で型を表す。

```go
var m1 map[int]string
m1 = make(map[int]string)
m1[1] = "US"
m1[81] = "Japan"
fmt.Println(m1) // map[1:US 81:Japan]

// リテラル
m2 := map[int]string{
    1: "Taro",
    2: "Jiro", // 行をまたぐ記載はカンマを忘れずに
}

// 要素の型を省略できる
m3 := map[int][]int{
    1: {1},
    2: {1, 2},
}

// 要素数取得
len(m3)

// 要素の除去
delete(m3, 2) // m3のkey:2を削除
```

#### マップの参照について
Goの基本型はnilのような特別な状態を持たない。
ゆえに以下のようなことが起こりうる。

```go
m := map[int]int{1: 0}
i := m[7] // i = 0
```

これを防ぐために、マップへのアクセスに以下の方法がある。

```go
m := map[int]string{
    1: "A",
    2: "B",
    3: "C",
}

s1, ok := m[1] // s1 = "A" ok = true
s1, ok := m[10] // s1 = "" ok = false

if _, ok := m[2]; ok {
    // m[2]が存在する場合
}
```

#### mapのforについて
keyの順序は保証されない。


### チャネル
ゴルーチン同士で、
データの受け渡しを司るために用意されたデータ構造。

キュー(FIFO)のような構造で、
送信と受信を非同期で行い、データのやり取りをする。

`chan データ型`の形式で型を表す。

```go
// 双方向チャネル
var ch chan int

// 受信専用チャネル
var ch1 <-chan int

// 送信専用チャネル
var ch2 chan<- int

// チャネルのオープン(下記はバッファサイズ20)
ch = make(chan int, 20)

// 送信
ch1 <- 5

// 受信
i := <-ch2

// 双方向チャネルは受信専用、送信専用共に代入できる
ch = ch1 // OK
ch = ch2 // OK
ch1 = ch2 // NG

// チャネルに溜まったデータの数を知る
len(ch)

// チャネルのバッファサイズを知る
cap(ch)

// チャネルのクローズ
close(ch)
ch <- 1 // 閉じたチャネルへの送信はランタイムパニック
i, ok := <-ch // バッファが空になっても初期値を受信し続ける
```

#### レシーバ
チャネルはゴルーチンを前提にした仕組みのため、
チャネルからデータを受信した場合に動くレシーバ関数を定義する必要がある。

```go
func reciever(ch <-chan int) {
	for {
		i, ok := <-ch

		// 受信できなくなったら終了
		if ok == false {
			break
		}
		
		// 受信したら出力
		fmt.println(i)
	}
}
```

#### 送信例
```go
func chanelSample() {
    // バッファは20(指定しないと0)
	ch := make(chan int, 20)

	go reciever(ch)

	i := 0
	for i < 100 {
		ch <- i
		i++
	}
	close(ch)

	// 実際は出力を待たないと、レシーバ関数の出力が最後まで行われない
}
```

#### チャネルの停止条件
・バッファサイズが0、または空のチャネルからの受信
or
・バッファサイズが0、または空きがないチャネルへの送信

#### selectを用いて、複数のチャネルを扱う
1ゴルーチンに複数のチャネルを受信する場合、
片方が停止すると後続処理が実行されなくなってしまう。

それを回避するために、以下のような記載を行う。

```go
select {
case e1 := <-ch1
    // ch1からの受信に成功した場合の処理
case e2, ok := <-ch2
    // ch2からの受信に成功した場合の処理
case ch3 <- e3:
    // ch3へe3を送信した場合
default:
    // それ以外の場合
}
```





## 演算子
### 文字列の結合
```go
s := "Yamada" + " " + "Taro"
```

### 算術演算子と代入(+=, *= ...)
使用可能。

### 等価
`==` `!=`

### 論理
`&&` `||`





## 関数
### 定義
```go
func 関数名([引数 引数型[, ...]]) [戻り値の型] {
    return[戻り値]
}
```

### 複数の値を返す
```go
func rawInt(a, b int) (int, int) {
    return a, b
}

func main() {
    reta, retb := rawInt(1, 2)
    
    _, retb2 :=  rawInt(3, 4) // _ を使うと、不要な戻り値を破棄することも可能
}
```

### 例外
特にそのような機構は無い。
関数が複数の値を返せることを利用して、自身で検知する必要あり。
```go
result, err := doSomething()
if (err != nil) {
    //
}
```

### 戻り値定義の省略
戻り値の型指定で、変数も初期値(0, falseなど)で定義してしまう。
```go
func rawInt(x int) (result int) {
    result += x
    return result
}
```

### 可変長引数
`...int`といった定義をかくと、
`スライスでまとめる`といった機能になる。

また、`s...`と記載すると、スライスの展開となり、
引数指定に用いることができる。

```go
func sum(s ...int) {
    //
}

s := []int{1,2,3}
sum(s...) // 6
```

### 無名関数
```go
func([引数 引数型[, ...]]) [戻り値の型] { return }
```

### init
パッケージの初期化を目的とした特殊な関数。
引数はもちろん、戻り値も指定できない。

また、複数登録できるという性質を持っている。
(コールする順序は出現順)

```go
func init() {
    //
}

func init() {
    // 複数指定も可
}
```





## 制御構文
### if
・`{}`の省略は不可
・必ずbool型を返す
・条件式の前に簡易文(式、代入、暗黙な変数定義など)が書ける
```go
if x, y := 1, 2; x < y { 
  // x, yはifのスコープになる
}
```

#### 例外処理テクニック
```go
if _, err := doSomething(); err != nil {
```

### for
・式の指定がない場合、無限ループ
・break、continueあり
・`range`を用いて、複数要素をもつデータ型を処理できる
```go
fluits := ["Apple", "Banana"]

for i, s:= range fluits {
    // i: インデックス
    // s: 要素
}
```

### switch
breakは不要。

```go
n := 3

case 1, 2 {
    case 1, 2:
        //
    case 3:
        //
    default
        //
}
}
```

#### switchで型アサーション
```go
// 値も欲しいときは `v := x.(type)` と書ける
switch x.(type) {
    case bool:
        //
    case int:
        //
    default:
        //
}
```

### goto
一応ある(使いたくない)

### defer
関数終了時に実行される関数を登録する。
複数回登録可能で、後から登録したものから呼ばれる。

```go
func run() {
    // [順3] 無名関数も可能
    defer func() {
        fmt.Println("closure")
    }() 

    defer fmt.Println("defer") // [順2]
    
    fmt.Println("done") // [順1]
}
```

### panicとrecover
一種の例外処理を実現する・・が、多用しないほうがいい。

#### panic
意図的にランタイムパニックを引き起こす。
panicにより関数は中断される。
deferは実行される。

```go
func main() {
    panic("runtime Error")
}
```
}

#### recover
panicにより引き起こされたランタイムパニックを回復する関数。
deferとセットで使用。

```go
func main() {
    defer func() {
        // xがnilでないなら、panicが実行されたことを示す
        if x := recover(); x != nul {
            fmt.Println(x)
        }
    }()
    
    panic("panic!")
}
```

### go
並行処理機能。
スレッドより小さい処理`ゴルーチン`によって、並行動作する。

defer同様、関数呼び出し形式の式を受け取る。
無名関数も使える。

```go
func sub() {
    // run sub
}

func main() {
    go sub()
    
    // run main
}
```





## ポインタ
値型のメモリ上のアドレスと型の情報のこと。
(参照型はそれ自体がポインタを使った参照を含んでいる)
`*型名`で定義する。

### アドレス演算子
任意の型から、そのポインタ型を生成できる。
```go
i := 3
p := &i // iのポインタ型を得る

// デリファレンス(メモリ上のアドレスを用いてデータ本体を参照)
i2 := *p // i2 = 3
```

### 配列型のデリファレンスは簡潔に書ける
```go
a := &[3]int{1, 2, 3}
p := &a

// こう書くべきだが・・・
fmt.Println((*p)[0]) // 1

// デリファレンスは不要でおk！(lenやcap、スライス式も同様)
fmt.Println(p[0]) // 1
```





## 構造体
複数の任意の方の値を1つにまとめたもの。
値型の一種。

### typeによるエイリアス 
`type エイリアス名 型`

### 構造体の定義、参照
structで定義して、typeで名前を与える。
再帰的な定義はできない。

タグと呼ばれるメタ情報を定義することもできる。
(文字列で定義するが、バッククォートによるRAW文字列が好まれる傾向がある)

```go
type 構造体名 struct {
    フィールド名 型 [`メタ情報`]
    ...
}
```

```go
type Point struct {
    X, Y int
}

vat pt Point

// 参照
pt.X // 0
pt.Y // 0

// 代入
pt.X = 10

// 複合リテラルによる初期化
pt := Point{
    X: 1,
    Y; 2,
}


// 型名の省略
type Z int
type PointWithZ {
    X, Y int
    Z // フィールド名と型名が同じであれば、型名を省略できる
}

// 中間フィールドの省略
type Numbers {
    one, two, three int
}

type Base {
    Numbers
}

base := Base{ Numbers: { one: 1 } }
base.one // フィールド名が一意であれば、中間フィールドは省略して参照できる

// ポインタからフィールドに直接アクセスできる
pbase := &base
pbase.one
```

### new
`new`を用いて、
型(主に構造体)のポインタ型を生成できる。

アドレス演算子を用いた初期化とほぼ一緒。

newの方が高速だが、実際はメモリ確保を行なっているだけのようで、
マップに対してnewを行なった場合は、Mapの初期化がなされない模様。
https://qiita.com/wannabe/items/87200a2cfc62cd7859bb

```go
type Person struct {
	age int
}

p := new(Person) // これと
p2 := &Person{} // これは、ほぼ一緒

fmt.Printf("%T, %v\n", p, p) // *main.Person, &{0}
fmt.Printf("%T, %v\n", p2, p2) // *main.Person, &{0}
```

### メソッド
Goにおいては、
任意の型に特化した関数を定義する仕組み。

パッケージの可視性同様、
`大文字開始の名前なら公開メソッド`
`小文字開始の名前なら非公開メソッド`
となる。

`func (レシーバ 型) 名前(...) {}`で定義できる。


```go
type MethodSample struct {
	X, Y int
}

func (m MethodSample) Sum() int {
	s := m.X + m.Y
	return s
}

func methodSample() {
	m := MethodSample{
		X: 1,
		Y: 2,
	}

	fmt.Println(m.Sum())
}
```

メソッドの実態は、レシーバを第１引数としてとる関数型のため、
以下のようなことも可能。

```go
f := (*MethodSample).Sum

f(&MethodSample{X: 1, Y: 2})
```


#### レシーバの型をポインタ型にする
参照渡しになり、変数を書き換えたりできる。

```go
func (m MethodSample) PlusOne() {
	m.X = m.X + 1
}

func (p *MethodSample) PlusOneRef() {
	p.X = p.X + 1
}

m := MethodSample{
	X: 1,
	Y: 2,
}

m.PlusOne()
fmt.Println(m) // 値渡しのため、Xは1のまま

m.PlusOneRef()
fmt.Println(m) // 参照渡しのため、Xは2になる
```

メソッドは書き方が特殊だが、レシーバを第１引数にする関数でしかないため、
以下のようなことも可能。

```go
// 第１引数にレシーバを指定するパターン
f := (*MethodSample).PlusOneRef

f(&MethodSample{X: 1, Y: 2})

// 第１引数不要(固定)パターン
p := &MethodSample{X: 1, Y: 2}
f := p.PlusOneRef // レシーバが固定された関数が得られる
f()
```

#### コンストラクタ
と言っても大した話ではない。

構造体の変数を初期化する際に、
`New***`みたいな関数名でやりましょってだけの話。

```go
type User struct {
	name string
	age  int
}

func NewUser(name string) *User {
	user := User{
		name: name,
		age:  0,
	}

	return &user // ポインタで返すのが肝
}
```




## インターフェース
他言語のあれに似ているが、

・インターフェースを実装するという定義自体は行わない
・インターフェースが定義するメソッドを実装するだけで良い

実装

```go
type インターフェース名 interface {
    メソッド名() 型
}
```

例

```go
// Sampleインターフェース
type Sample interface {
	ToString()
}

// 構造体Imp
type Imp struct {
	id int
}

// ImpはSmapleとして使いたいので、ToStringを実装
func (i *Imp) ToString() {
	fmt.Printf("%v", i.id)
}

// Sampleインターフェースを使う関数
func interfaceSample(s Sample) {
	s.ToString()
}

// ImpはToStringを実装しているため、Sampleとして渡せる
interfaceSample(&Imp{id: 1})
```

### 別のインターフェース定義を含ませる
```go
type I0 interface {
    Method1() int
}

type I1 interface {
    I0
    Method2() int
}

// I1を実装する場合、Method1, Method2の二つが必要
```

