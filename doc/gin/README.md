# 参考
[公式](https://github.com/gin-gonic/gin)

# 起動
```go
import "github.com/gin-gonic/gin"

...

func main() {
    // ルータの作成
	router := gin.Default()
	
	// 各種メソッドとパスの設定
	router.GET("/someGet", getting)
	router.POST("/somePost", posting)
	router.PUT("/somePut", putting)
	router.DELETE("/someDelete", deleting)
	router.PATCH("/somePatch", patching)
	router.HEAD("/someHead", head)
	router.OPTIONS("/someOptions", options)
	
	// 起動
	router.Run() // ポートを変えたい場合はrouter.Run(":ポート番号")
}
```

## ミドルウェアのないルータを作成
```go
router := gin.New()
```

※ `gin.Default()`にはLogger、Recoveryミドルウェアが設定されている

# パラメータ
## パスパラメータ
`:パラメータ名`は、解析で省略できない。
`*パラメータ名`は、解析で省略できる。

```go
// `/user/名前`でのみマッチ
router.GET("/user/:name", func(c *gin.Context) {
    name := c.Param("name")
    
    ...
})

// `/user/名前/アクション`と`/user/名前/`でマッチ
router.GET("/user/:name/*action", func(c *gin.Context) {
    name := c.Param("name")
    action := c.Param("action")
    
    ...
})
```

## クエリパラメータ 
パス部には特に記載しないでOK.
コンテキストから取得する模様。

```go
router.GET("/", func(c *gin.Context) {
    // firstnameの取得。指定がない場合、"Guest"を代入
	firstname := c.DefaultQuery("firstname", "Guest")
	
	// lastnameの取得。指定がない場合、空
	lastname := c.Query("lastname")
	
	// マップのパラメータの場合
    ids := c.QueryMap("ids")

	...
})
```

## Postパラメータ
```go
message := c.PostForm("message")
nick := c.DefaultPostForm("nick", "anonymous")

// マップのパラメータの場合
ids := c.PostFormMap("ids")
```

## ファイル
```go
file, _ := c.FormFile("file")

// 複数ファイル
form, _ := c.MultipartForm()
files := form.File["upload[]"]
```

# グルーピング
```go
v1 := router.Group("v1")

{
    // `/v1/1`でマッチ
	v1.GET("/1", func(c *gin.Context) {
        ...
	})
	// `/v1/2`でマッチ
	v1.GET("/2", func(c *gin.Context) {
		...
	})
}
```

# ミドルウェアの設定
```go
router.USE(ミドルウェア)

// グルーピング作成時に設定
authorized := r.Group("/", ミドルウェア)

// グループに対してUSEで設定
v1 := r.Group("/v1")
v1.USE(ミドルウェア)
```