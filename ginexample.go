package main

import "github.com/gin-gonic/gin"

func main() {
	router := gin.Default()
	router.GET("/", func(c *gin.Context) {
		firstname := c.DefaultQuery("firstname", "Guest")
		lastname := c.Query("lastname")
		c.JSON(200, gin.H{
			"firstname": firstname,
			"lastname":  lastname,
		})
	})
	router.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	v1 := router.Group("v1")

	{
		v1.GET("/1", func(c *gin.Context) {
			c.JSON(200, gin.H{
				"message": "route1",
			})
		})
		v1.GET("/2", func(c *gin.Context) {
			c.JSON(200, gin.H{
				"message": "route2",
			})
		})
		v1.GET("/3", func(c *gin.Context) {
			c.JSON(200, gin.H{
				"message": "route3",
			})
		})
	}

	router.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
